package footballstat.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.geometry.Pos;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Player {

    enum Position{
        GOALKEEPER, DEFENDER, MIDDLEFIELDER, FORWARD
    }

    private @Id @GeneratedValue Long id;

    @Column(unique = true)
    private String name;

    @Column
    private Position position;

    @Column
    private String teamTitle;

    @Column
    private String nationality;

    @Column
    private Date dateOfBirth;

    @Column
    @OneToMany(mappedBy="player", cascade = CascadeType.ALL)
    private List<Goal> goals;

    @ManyToOne
    @JoinColumn(name = "team_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Team team;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public Team getTeam() {
        return team;
    }

    public String getTeamTitle() {
        return teamTitle;
    }

    public void setTeamTitle(String teamTitle) {
        this.teamTitle = teamTitle;
    }
}
