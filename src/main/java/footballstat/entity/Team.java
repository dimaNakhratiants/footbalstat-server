package footballstat.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Team {

    private @Id @GeneratedValue Long id;

    @Column(unique = true)
    private String title;

    @OneToMany(mappedBy="team", cascade = CascadeType.ALL)
    private List<Player> players;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
