package footballstat.repositories;

import footballstat.entity.Fixture;
import footballstat.entity.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FixtureRepository extends CrudRepository<Fixture, Long> {

    List<Fixture> findByHomeTeam(Team hometeam);
    List<Fixture> findByAwayTeam(Team awayteam);

}
