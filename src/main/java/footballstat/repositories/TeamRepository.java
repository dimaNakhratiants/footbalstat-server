package footballstat.repositories;

import footballstat.entity.Player;
import footballstat.entity.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository  extends CrudRepository<Team, Long> {

    Team findByTitle(String title);

}
