package footballstat.repositories;

import footballstat.entity.Goal;
import footballstat.entity.Player;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GoalRepository extends CrudRepository<Goal, Long> {

    List<Goal> findByPlayer(Player player);

}
