package footballstat.repositories;

import footballstat.entity.Player;
import footballstat.entity.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    List<Player> findByTeam(Team team);
    Player findByName(String name);

}
