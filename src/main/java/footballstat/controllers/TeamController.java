package footballstat.controllers;


import footballstat.entity.Team;
import footballstat.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TeamController {

    @Autowired
    TeamRepository repository;

    @RequestMapping("/teams")
    public ResponseEntity<ArrayList<Team>> getAll(){
        List<Team> teams = (List<Team>) repository.findAll();
        return new ResponseEntity(teams, HttpStatus.OK);
    }

    @RequestMapping(value = "/team", method = RequestMethod.POST)
    public ResponseEntity<Team> add(@RequestBody Team team){
        Team newTeam = repository.save(team);
        return new ResponseEntity(newTeam, HttpStatus.OK);
    }

    @RequestMapping(value = "/team/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestBody Team team){

        repository.delete(team.getId());

        return new ResponseEntity(team, HttpStatus.OK);
    }

}