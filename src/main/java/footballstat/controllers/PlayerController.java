package footballstat.controllers;


import footballstat.entity.Player;
import footballstat.entity.Team;
import footballstat.repositories.PlayerRepository;
import footballstat.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PlayerController {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;

    @RequestMapping("/players")
    public ResponseEntity<ArrayList<Player>> getAll(){
        List<Player> players = (List<Player>) playerRepository.findAll();
        return new ResponseEntity(players, HttpStatus.OK);
    }

    @RequestMapping(value = "/player", method = RequestMethod.POST)
    public ResponseEntity<Player> add(@RequestBody Player player){
        Team team = teamRepository.findOne(player.getTeam().getId());
        player.setTeamTitle(team.getTitle());
        Player newPlayer = playerRepository.save(player);
        return new ResponseEntity(newPlayer, HttpStatus.OK);
    }

    @RequestMapping(value = "/player/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestBody Player player){
        playerRepository.delete(player.getId());
        return new ResponseEntity(player, HttpStatus.OK);
    }

}
