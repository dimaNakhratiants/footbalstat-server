package footballstat.controllers;

import footballstat.entity.Fixture;
import footballstat.entity.Goal;
import footballstat.entity.Player;
import footballstat.repositories.GoalRepository;
import footballstat.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GoalController {

    @Autowired
    GoalRepository repository;

    @Autowired
    PlayerRepository playerRepository;

    @RequestMapping("/goals")
    public ResponseEntity<ArrayList<Goal>> getAll(@RequestParam(value = "player", required = false) String player) {
        System.out.println(player);
        if (player == null) {
            List<Goal> goals = (List<Goal>) repository.findAll();
            return new ResponseEntity(goals, HttpStatus.OK);
        }else{
            Player playerKey = playerRepository.findByName(player);
            List<Goal> goals = repository.findByPlayer(playerKey);
            return new ResponseEntity(goals, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/goal", method = RequestMethod.POST)
    public ResponseEntity<Fixture> add(@RequestBody Goal goal) {
        String name = playerRepository.findOne(goal.getPlayer().getId()).getName();
        goal.setPlayerName(name);
        Goal newGoal = repository.save(goal);
        return new ResponseEntity(newGoal, HttpStatus.OK);
    }

    @RequestMapping(value = "/goal/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestBody Goal goal) {
        repository.delete(goal.getId());
        return new ResponseEntity(goal, HttpStatus.OK);
    }
}
