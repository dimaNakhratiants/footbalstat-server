package footballstat.controllers;

import footballstat.entity.Fixture;
import footballstat.entity.Team;
import footballstat.repositories.FixtureRepository;
import footballstat.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
public class FixtureController {

    @Autowired
    FixtureRepository repository;

    @Autowired
    TeamRepository teamRepository;

    @RequestMapping(value = "/fixtures")
    public ResponseEntity<ArrayList<Fixture>> getByTeam(@RequestParam(value = "team", required = false) String team) {
        if (team != null) {
            Team keyTeam = teamRepository.findByTitle(team);
            ArrayList<Fixture> fixtures = (ArrayList<Fixture>) repository.findByHomeTeam(keyTeam);
            fixtures.addAll(repository.findByAwayTeam(keyTeam));
            return new ResponseEntity<ArrayList<Fixture>>(fixtures, HttpStatus.OK);
        } else {
            ArrayList<Fixture> fixtures = (ArrayList<Fixture>) repository.findAll();
            return new ResponseEntity<ArrayList<Fixture>>(fixtures, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/fixture", method = RequestMethod.POST)
    public ResponseEntity<Fixture> add(@RequestBody Fixture fixture) {
        Fixture newFixture = repository.save(fixture);
        return new ResponseEntity(newFixture, HttpStatus.OK);
    }

    @RequestMapping(value = "/fixture/delete", method = RequestMethod.POST)
    public ResponseEntity delete(@RequestBody Fixture fixture) {
        repository.delete(fixture.getId());
        return new ResponseEntity(fixture, HttpStatus.OK);
    }


}
