package footballstat.responses;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.xml.internal.ws.developer.Serialization;
import footballstat.entity.Fixture;
import footballstat.entity.Goal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FixtureResponse {

    long id;

    private String homeTeam;
    private String awayTeam;
    private int homeGoals;
    private int awayGoals;
    private Date date;
    private List<GoalResponse> goals = new ArrayList();

    public FixtureResponse(Fixture fixture) {
        this.id = fixture.getId();
        this.homeTeam = fixture.getHomeTeam().getTitle();
        this.awayTeam = fixture.getAwayTeam().getTitle();
        this.homeGoals = fixture.getHomeGoals();
        this.awayGoals = fixture.getAwayGoals();
        this.date = fixture.getDate();

        for (Goal goal:fixture.getGoals()) {
            this.goals.add(new GoalResponse(goal));
        }
    }
}
