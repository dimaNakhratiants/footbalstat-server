package footballstat.responses;

import footballstat.entity.Goal;

public class GoalResponse {

    private String player;
    private byte minute;

    public GoalResponse(Goal goal) {
        this.player = goal.getPlayer().getName();
        this.minute = goal.getMinute();
    }
}
